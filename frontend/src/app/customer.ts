export class Customer {

  constructor(
      public studentId: string,
      public FAFSA: boolean,
      public SNAP: boolean,
      public WIC: boolean,
      public SSI: boolean,
      public TAFDC: boolean,
      public MRC: boolean,
      public SNAPAssistance: boolean,
      public currentHousingSituation: string,
      public annualIncome: boolean,
      public currentHouseholdSize: number,
      public ageGroup1: number,
      public ageGroup2: number,
      public ageGroup3: number,
      public ageGroup4: number,
      public zipCode: string,
      public primaryIncomeSource: string,
      public gender: string
  ) { }

}
